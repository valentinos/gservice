/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Application Main
 * @author Valentinos Galanos <swg_lowraed@hotmail.com>
 * @version 1.0.0
 */
(function(win){
    var window = win;
    var $ = window.jQuery;
    var ko = window.ko;
    
    // CORE
    window.JSAPP = window.JSAPP || {};
    var CORE = window.JSAPP;
    CORE['includes'] = [
        'js/lib/customBidings.js',
        'js/lib/navbar.js',
        'js/lib/storage.js',
        'js/lib/services.js'
    ];
    CORE['observableObj'] = function(obj, model) {
        for(var prop in obj) {
            model[prop] = ko.observable(obj[prop]);
        }
    };
    CORE['defaults'] = {
        serviceUrl: 'http://localhost/gservice/index.php/backbone',
    };
    
    function loadApplication(baseUrl) {
        // Include Requirements.
        var $head = $('head title');
        for(var i=0, l=CORE.includes.length; i < l; ++i) {
            var $script = $('<script>').attr({
                src: (baseUrl+'/' || '') + CORE.includes[i]
            });
            $head.after($script);
        }
        applicationRun(baseUrl);
    }
    
    function bindKnockout(app) {
        ko.applyBindings(app);
    }
    
    /**
     * APPLICATION RUN
     */
    
    function applicationRun(baseUrl) {
        var navbar = new CORE.NavigationBar();
        var serviceManager = new CORE.ServiceManager(baseUrl);
        var homePage = {
            showed: ko.observable(true)
        };
        
        navbar.addMenuItem({
            label: 'Create New Service',
            click: function(){
                navbar.nav()[1].click();
                serviceManager.createNewService();
            }
        });
        navbar.addBarItem({
            label: null,
            icon: 'glyphicon-home',
            active: true,
            click: function(ev) {
                serviceManager.showed(false);
                homePage.showed(true);
            }
        });
        navbar.addBarItem({
            label: 'services',
            icon : 'glyphicon-cog',
            click: function(ev) {
                homePage.showed(false);
                serviceManager.showed(true);
            }
        });
        
        var appView = {
            navbar: navbar,
            serviceManager: serviceManager,
            homePage: homePage
        };        
        bindKnockout(appView);
    }
    
    
    CORE['run'] = loadApplication;
})(this);
