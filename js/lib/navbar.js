/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Javascript File
 * @author Valentinos Galanos <swg_lowraed@hotmail.com>
 * @version 1.0.0
 */
(function(win){
    var $ = win.jQuery;
    var ko = win.ko;
    var CORE = win.JSAPP;
    
    
    var NavigationBar = function() {
        var self=this;
        self.brand = ko.observable('GService');
        self.menuLabel = ko.observable('Q');
        self.menuItems = ko.observableArray();
        self.nav = ko.observableArray();
        
        self.addMenuItem = function(o) {
            var n = new NavMenuItem(o);
            self.menuItems().push(n);
        };
        
        self.addBarItem = function(o) {
            var n = new NavBarItem(o,self);
            self.nav().push(n);
        };
    };
    
    var NavBarItem = function(obj, navbar) {
        var self = this;
        var o = $.extend({
            click:function(){},
            icon: null,
            label: 'label',
            active: false
        },obj||{});  
        
        self.click = function(ev){
            if(o.click!==undefined) {
                o.click.call(self,ev);
            }
            $(navbar.nav()).each(function(){
                this.isActive(false);
            });
            self.isActive(true);
        };
        self.icon = ko.observable(o.icon);
        self.label = ko.observable(o.label);
        self.isActive = ko.observable(o.active);
        self.css = ko.computed(function(){
            return (self.isActive()==true) ? 'active' : '';
        });
    };
    
    var NavMenuItem = function(o) {
        var self=this;
        var o = $.extend({
                label: '', 
                url:'#', 
                style:'',
                click: function(){}
            }, o||{});
            
        self.label = ko.observable(o.label);
        self.url = ko.observable(o.url);
        self.style = ko.observable(o.style);
        self.click = function(ev){
            o.click.call(this,ev);
        };
    };
    
    win.JSAPP['NavigationBar'] = NavigationBar;
    win.JSAPP['NavBarItem'] = NavBarItem;
    win.JSAPP['NavMenuItem']   = NavMenuItem;
    
})(this);
