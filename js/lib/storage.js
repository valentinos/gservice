/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Storage Manager
 * Needs: jquery.cookie.js for cookie storage support.
 * 
 * @author Valentinos Galanos <swg_lowraed@hotmail.com>
 * @version 1.0.0
 */
(function(win){
    var ko = win.ko;
    var $ = win.jQuery;
    win.JSAPP = win.JSAPP || {};
    
    var HTMLStorage = function(manager) {
        var local = win.localStorage;
        var session = win.sessionStorage;
        var self = this;
        
        self.saveObj = function(name, o) {
            var json = ko.toJSON(o);
            local.setItem(name, json);
        };
        self.loadObj = function(name) {
            var i = local.getItem(name);
            if(i!==undefined) {
                var json = local.getItem(name);
                return win.JSON.parse(json);
            } else {
                return undefined;
            }
        };
    };
    
    var CookieStorage = function(manager) {
        var store, self=this;
        function initStore(){store = JSON.parse($.cookie('__a_store')) || {};}
        function saveStore(){$.cookie('__a_store', JSON.stringify(store));}
        self.saveObj = function(name, o) {
            initStore();
            store[name] = ko.toJSON(o);
            saveStore();
        };
        self.loadObj = function(name) {
            initStore();
            return store[name];
        };
    };
    
    var StorageManager = function() {
        var storage, self = this;
        if(typeof('Storage') !== undefined) {
            storage = new HTMLStorage(self);
        } else {
            storage = new CookieStorage(self);
        }
        self.storeModel = function(name, model) {
            storage.saveObj(name, model);
        };
        self.loadModel = function(name, callback) {
            var obj = storage.loadObj(name);
            if(callback) callback.call(this,obj);
        };
    };
    
    var CloudStorageManager = function() {
        var xhr, self=this;
        
        function sendjson(o) {
            var options = $.extend({
                cache:false,
                type:'POST',
                dataType:'json'
            }, o||{});
            
            if(xhr!==undefined && xhr.readyState!==4) {
                xhr.abort();
            }
            
            xhr = $.ajax(options);
        };
        
        
        self.storeModel = function(type, data, callback) {
            var json = win.JSON.stringify({
                type:type,
                data: ko.toJS(data)
            });
            sendjson({
                url: win.JSAPP.defaults.serviceUrl + '/store',
                data: json,
                success: function(res){
                    if(callback!==undefined) callback(res);
                }
            });
        };
        
        self.removeModel = function(type, data, callback) {
            var json = win.JSON.stringify({
                type: type,
                data: ko.toJS(data)
            });
            sendjson({
                url: win.JSAPP.defaults.serviceUrl + '/remove',
                data: json,
                success: function(res) {
                    callback.call(self,res);
                }
            });
        };
        
        self.loadModels = function(type, callback) {
            var json = win.JSON.stringify({
                type: type
            });
            sendjson({
                url: win.JSAPP.default.serviceUrl + '/load',
                data: json,
                success: function(res) {
                    if(callback!==undefined)
                        callback.call(self, res);
                }
            });
        };
        
        self.storeModels = function(type, models, callback) {
            var a = [];
            $(models()).each(function(i){
                a.push(ko.toJS(this));
            });            
            var json = win.JSON.stringify({
                type: type,
                data: a
            });            
            sendjson({
                url: win.JSAPP.defaults.serviceUrl + '/store',
                data: json,
                success: function(res) {
                    if(callback!==undefined) callback(res);
                }
            });
        };
        
        self.getControllers = function(callback) {
            var json = win.JSON.stringify({client:'one'});
            sendjson({
                url: win.JSAPP.defaults.serviceUrl + '/controllers',
                data: json,
                success: function(res) {
                    if(callback!==undefined) callback.call(self,res);
                }
            });
        };
        
        /**
         * Call Action from Yii Controller
         * @param {string} action
         * @param {string} type
         * @param {obj} data
         * @param {function} response
         * @returns {undefined}
         */
        self.callAction = function(action, type, data, response) {
            var json = win.JSON.stringify({
                'type' : type,
                'data' : data
            });
            sendjson({
                url: win.JSAPP.defaults.serviceUrl + '/' + action,
                data: json,
                success: function(res) {
                    if(response!==undefined) response.call(self,res);
                }
            });
        };
    };
    
    win.JSAPP['StorageManager'] = StorageManager;
    win.JSAPP['CloudManager'] = CloudStorageManager;
    
})(this);
