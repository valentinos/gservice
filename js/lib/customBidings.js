/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Javascript File
 * @author Valentinos Galanos <swg_lowraed@hotmail.com>
 * @version 1.0.0
 */
(function(win){
    var window = win;
    var ko = win.ko;
    var $ = win.jQuery;
    
    // Helpers
    
    function unwrap(observ) {
        var value = ko.unwrap(observ);
        return value;
    }
    
    // Fade In/Out Binding Effect
    
    ko.bindingHandlers.fade = {
        init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
            // This will be called when the binding is first applied to an element
            // Set up any initial state, event handlers, etc. here
            var value = unwrap(valueAccessor());
            var $el = $(element);
            
            if(value) {
                $el.fadeIn(100);
            } else {
                $el.fadeOut(100);
            }
        },
        update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
            // This will be called once when the binding is first applied to an element,
            // and again whenever the associated observable changes value.
            // Update the DOM element based on the supplied values here.
            var value = unwrap(valueAccessor());
            var $el = $(element);
            
            if(value) {
                $el.fadeIn(400);
            } else {
                $el.fadeOut(400);
            }
        }
    };
    
})(this);
