/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Javascript File
 * @author Valentinos Galanos <swg_lowraed@hotmail.com>
 * @version 1.0.0
 */
(function(win){
    var $ = win.jQuery;
    var ko = win.ko;
    
    
    var ServiceManager = function(baseUrl) {
        var self = this;
        var storage = new win.JSAPP.StorageManager();
        var cloud = new win.JSAPP.CloudManager();

        self.cloud = cloud;
        self.services = ko.observableArray();
        self.selectedService = ko.observable();        
        self.showed = ko.observable(false);
        self.baseUrl = baseUrl || '';
        self.controllers = ko.observableArray();
        
        cloud.getControllers(function(res){
            self.controllers(res);
        });
        
        // Create New Service
        self.createNewService = function() {
            var s = new Service({},self);
            cloud.getControllers(function(res){
                s.controllers(res);
            });
            s.select();
        };
        // Remove Service
        self.removeService = function(service) {
            var services = self.services;
            var idx = services.indexOf(service);
            if(idx !== -1) {
                var r = services.splice(idx,1);
            }
            self.selectedService(null);
            self.save();
        };
        // Disable / Enable All
        self.disableAll = function() {
            $.each(self.services(), function(){
                this.disable();
            });
            self.save();
        };
        self.enableAll = function() {
            $.each(self.services(), function(){
                this.enable();
            });
            self.save();
        };
        // Check out services
        self.allDisabled = ko.computed(function(){
            var is=false;
            $.each(self.services(), function(){
                var on = !this.isRunning();
                if(!on) {
                    return false;
                } else {
                    is = on;
                }
            });
            return is;
        });
        
        self.save = function() {
            storage.storeModel('Services', self.services);
            cloud.storeModels('Service', self.services, function(res){
                var syncModels = res;
                self.services.removeAll();
                $.each(syncModels,function(){
                    var n = new Service(this, self);
                    self.services.push(n);
                });
                storage.storeModel('Services', self.services);
            });
        };
        
        self.load = function() {
            storage.loadModel('Services',function(obj){                
                if(obj!==null) {
                    $.each(obj,function(){
                        var n = new Service(this, self);
                        self.services.push(n);
                    });
                }
            });
        };
        
        self.load();
    };
    
    var Service = function(obj, manager) {
        var self=this;
        var o = $.extend({
            name: 'Service #' + (manager.services().length+1),
            css: '', 
            description:'',
            createdAt:win.Math.round(new Date().getTime() / 1000),
            controller: null,
            isRunning:false, isNew:true, isSaved:false
        },obj||{});
        
        self.pk=ko.observable(o.pk);
        self.name=ko.observable(o.name);
        self.css=ko.observable(o.css);
        self.isRunning=ko.observable(o.isRunning);
        self.description=ko.observable(o.description);
        self.createdAt=ko.observable(o.createdAt);
        self.controller=ko.observable(o.controller);
        self.isSaved=ko.observable(o.isSaved);
        self.isNew=ko.observable(o.isNew);
        self.code=ko.observable();
        self.codeCss=ko.observable();
        self.fullBtn=ko.observable('Full View');
        
        function getCode() {
            manager.cloud.callAction('controller',self.controller(),null,function(res){
                if(res.data!==undefined && res.data!==null) {
                    self.code(res.data);
                    // Init Syntax
                    $('pre code').each(function(i, e) {
                        var html = $(e).html();
                        //var a = win.hljs.highlightAuto(html);
                        var b = win.hljs.highlightBlock(e);
                    });
                }
            });
        }
        
        
        self.delete = function(o, ev){
            $(ev.target).parents('.list-group-item').fadeOut(function(){
                manager.cloud.removeModel('Service', self, function(res){
                    if(res.removed) {
                        manager.removeService(self);
                        manager.save();
                    }
                });
            });
        };
        self.select = function(){
            $(manager.services()).each(function(){
                this.css('');
            });
            self.css('active');
            manager.selectedService(self);
            getCode();
        };
        self.enable = function() {
            self.isRunning(true);
            manager.save();
        };
        self.disable = function(o, ev) {
            self.isRunning(false);
            manager.save();
        };
        self.save = function(o, ev) {
            self.css('');
            if(self.isNew()) {
                manager.services.push(self);
            }
            self.isNew(false);
            self.isSaved(false);
            manager.save();
            self.isSaved(true);
            $('.notifications').notify({
                message: {
                    text: 'Service "' + self.name() + '" saved successfully'
                }
            }).show();
            setTimeout(function(){self.isSaved(false);}, 2000);
        };
        self.cancel = function() {
            manager.selectedService(null);
        };
        self.showFullCode = function(e) {
            if(self.codeCss() === 'fullscreen') {
                self.codeCss('');
                self.fullBtn('Full View');
            } else {
                self.codeCss('fullscreen');
                self.fullBtn('Simple View');
            }
        };
    };
    
    win.JSAPP['ServiceManager'] = ServiceManager;
    win.JSAPP['Service']   = Service;
    
})(this);
