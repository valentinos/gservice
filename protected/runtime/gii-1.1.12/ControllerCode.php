<?php
return array (
  'template' => 'default',
  'baseClass' => 'ServiceController',
  'actions' => 'auth,store,load',
);
