<?php /* @var $this Controller */ 
$base = Yii::app()->baseUrl;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

        <link rel="stylesheet" href="<?php echo $base;?>/js/ext/jquery/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $base;?>/js/ext/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo $base;?>/js/ext/bootstrap/bootstrap-notify.css">
        <link rel="stylesheet" href="<?php echo $base;?>/css/main.css">
        <link rel="stylesheet" href="<?php echo $base;?>/js/ext/highlights/styles/default.css">
        
        
        <script src="<?php echo $base;?>/js/ext/jquery/jquery.js"></script>
        <script src="<?php echo $base;?>/js/ext/jquery/jquery-ui.js"></script>
        <script src="<?php echo $base;?>/js/ext/jquery/jquery.cookie.js"></script>
        
        <script src="<?php echo $base;?>/js/ext/highlights/highlight.js"></script>
        
        <script src="<?php echo $base;?>/js/ext/bootstrap/js/bootstrap.js"></script>
        <script src="<?php echo $base;?>/js/ext/bootstrap/bootstrap-notify.js"></script>
        
        <script src="<?php echo $base;?>/js/ext/knockout/knockout.debug.js"></script>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
    
    <?php echo $this->renderPartial('//layouts/navbar');?>
    <div class="container">
        <?php echo $content; ?>
    </div>
    
    <div class='notifications bottom-right'></div>
    <script src="<?php echo $base;?>/js/app.js"></script>
    <script>(function(win){
        var $ = win.jQuery;
        //win.hljs.initHighlightingOnLoad();
        $(win.document).ready(function(){
            win.JSAPP.run("<?php echo Yii::app()->baseUrl;?>");
        });
    })(window);</script>
</body>
</html>
