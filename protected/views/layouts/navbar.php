<?php
/* @var $this Controller */
?>
<nav class="navbar navbar-default" role="navigation" data-bind="with: navbar">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a class="navbar-brand" href="#" data-bind="text:$data.brand">Brand</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span data-bind="text:$data.menuLabel"></span><b class="caret"></b></a>
            <ul class="dropdown-menu" data-bind="foreach:$data.menuItems">
            <li><a href="#"
                   data-bind="text:$data.label,click:$data.click,
                   attr:{href: $data.url,class: $data.style};
                   ">Action</a></li>
            </ul>
        </li>
        <!-- ko foreach: $data.nav -->        
        <li data-bind="css: $data.css">
            <a href="#" data-bind="click: $data.click">
                <span class="glyphicon" 
                      data-bind="css: $data.icon, visible:$data.icon"></span>
                <span data-bind="text: $data.label, visible:$data.label"></span>
            </a>
        </li>
        <!-- /ko -->
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>