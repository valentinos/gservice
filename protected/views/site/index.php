<?php
/* @var $this SiteController */
$this->pageTitle = Yii::app()->name;
?>
<div class="row" data-bind="with: homePage, fade: $root.homePage.showed">
    <div class="col-md-12">
        <table class="table table-condensed">
            <thead>
            <th>Service</th><th>Controller</th><th>Status</th>
            </thead>
            <tbody data-bind="foreach: $root.serviceManager.services">
                <tr>
                    <td data-bind="text:$data.name"></td>
                    <td data-bind="text:$data.controller"></td>
                    <td>
                        <span class="label label-info" data-bind="visible:$data.isRunning">Running...</span>
                        <span class="label label-warning" data-bind="visible:$data.isRunning() === false">Stopped...</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row" data-bind="with: serviceManager, fade: $root.serviceManager.showed">
    <div class="col-md-4">

        <div class="panel panel-default">
            <div class="panel-heading">Services

                <a href="#" data-bind="click: $data.createNewService"
                   class="pull-right">
                    <i class="glyphicon glyphicon-plus"></i>
                    Create Service
                </a>
                <a href="#" data-bind="click: $data.disableAll, visible:!$data.allDisabled()"
                   class="pull-right" style="margin-right:10px;">
                    <i class="glyphicon glyphicon-stop"></i>
                    Disable All</a>
                <a href="#" data-bind="click: $data.enableAll, visible:$data.allDisabled()"
                   class="pull-right" style="margin-right:10px;">
                    <i class="glyphicon glyphicon-play"></i>
                    Enable All</a>
            </div>
            <div class="panel-body">
                <div class="list-group" data-bind="foreach: $data.services">
                    <div class="list-group-item" data-bind="css:$data.css, click:$data.select">
                        <div style="overflow:auto;">
                            <h5 data-bind="text:$data.name" style="float:left"></h5>

                            <div style="float:right;">
                                <a href="#" class="btn mini"
                                   data-bind="click:$data.delete">
                                    <i class="glyphicon glyphicon-remove"></i></a>

                                <span class="label label-info" data-bind="visible:$data.isRunning">&Rightarrow;</span>
                                <span class="label label-warning" data-bind="visible:$data.isRunning() === false">&bigcirc;</span>
                                <div class="dropdown" style="position: absolute;top: 17px;right: 78px;">
                                    <A href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></A>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#"
                                                                   data-bind="click:$data.disable, visible:$data.isRunning()">Disable</a></li>
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#"
                                                                   data-bind="click:$data.enable, visible:!$data.isRunning()">Enable</a></li>
                                        <li role="presentation" class="divider"></li>
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#"
                                                                   data-bind="click:$data.delete">Remove</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-8">

        <div class="panel panel-default" 
             data-bind="with: $data.selectedService, fade: $data.selectedService()">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left" data-bind="text:$data.name"></h3>
                <div class="pull-right">
                    <span class="label label-success" 
                          data-bind="visible:$data.isSaved">Saved</span>
                    <span class="label label-primary"
                          data-bind="visible:$data.isNew">New</span>
                </div>
            </div>
            <div class="panel-body">
                <form class="form form-horizontal service_form">
                    <div class="col-md-12">
                        <label>Service Name:</label>
                        <input data-bind="value:$data.name" class="form-control">
                        <label>Controller:</label>
                        <input data-bind="value:$data.controller,
                               enable:$data.isNew(),
                               visible:$data.isNew()==false" class="form-control">
                        <select data-bind="
                                options: $parent.controllers,
                                value: $data.controller,
                                visible: $data.isNew()"
                                class="form-control">
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>Created Date:</label>
                        <input data-bind="value:$data.createdAt,enable:false" type="text" class="form-control">
                    </div>
                    <div class="col-md-8">
                        <label>Description:</label>
                        <textarea data-bind="value:$data.description" class="form-control"></textarea>
                    </div>

                    <div class="col-md-12" style="margin-top: 20px;">

                        <div class="code-panel" data-bind="css: $data.codeCss, visible: $data.isNew()==false">
                            <a href="#" class="btn" data-bind="click: $data.showFullCode">
                                    <span class="glyphicon glyphicon-expand"></span>
                                    <span data-bind="text: $data.fullBtn"></span>
                            </a>
                            <pre class="code" style="height: 400px;overflow:hidden;">    
                                <code data-bind="text:$data.code"></code>
                            </pre>
                        </div>
                        
                    </div>

                    <div class="col-md-12 well well-sm" style="margin-top:10px;text-align:center;">
                        <button data-bind="click: $data.save" 
                                class="btn btn-primary">Save</button>
                        <button data-bind="visible:$data.isNew(), click: $data.cancel" 
                                class="btn btn-primary">Cancel</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>