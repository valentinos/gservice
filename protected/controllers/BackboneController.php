<?php

class BackboneController extends ServiceController {

    public $payload;

    public function filters() {
        return array(
            'ReguestJSON', 'ClientAuth'
        );
    }

    /**
     * Request JSON Filter
     * @param CFilterChain $filterChain
     */
    public function filterReguestJSON($filterChain) {
        $payload = file_get_contents('php://input');
        if ($payload) {
            $this->payload = $payload;
        } else {
            Yii::app()->end();
        }
        $filterChain->run();
    }

    /**
     * Client Auth JSON Filter
     * @param CFilterChain $filterChain
     */
    public function filterClientAuth($filterChain) {
        $filterChain->run();
    }

    public function actionLoad() {
        $msg = CJSON::decode($this->payload);
    }

    public function actionRemove() {
        $msg = CJSON::decode($this->payload);
        if (array_key_exists('type', $msg)) {
            $data = $msg['data'];
            if (isset($data['list'])) {
                foreach ($data as $row) {
                    if (isset($row['pk'])) {
                        $pk = $row['pk'];
                        $data['removed'] = $this->removeModel($msg['type'], $pk);
                    }
                }
            } else {
                if (isset($data['pk'])) {
                    $pk = $data['pk'];
                    $data['removed'] = $this->removeModel($msg['type'], $pk);
                }
            }
            header('Content-Type: application/json;charset=UTF-8');
            echo CJSON::encode($data);
        }
    }

    public function actionStore() {
        $msg = CJSON::decode($this->payload);
        if (array_key_exists('type', $msg)) {
            $data = $msg['data'];
            if (is_array($data)) {
                foreach ($data as &$row) {
                    // Update
                    if (isset($row['pk'])) {
                        $pk = $row['pk'];
                        if (!$this->updateModel($msg['type'], $row, $pk)) {
                            $id = $this->storeModel($msg['type'], $row);
                            if ($id)
                                $row['pk'] = $id;
                        }
                    } else {
                        // Create
                        $id = $this->storeModel($msg['type'], $row);
                        if ($id)
                            $row['pk'] = $id;
                    }
                }
            } else {
                // Update
                if (isset($data['pk'])) {
                    if (!$this->updateModel($msg['type'], $data, $data['pk'])) {
                        $id = $this->storeModel($msg['type'], $data);
                        if ($id)
                            $data['pk'] = $id;
                    }
                } else {
                    // Create
                    $id = $this->storeModel($msg['type'], $data);
                    if ($id)
                        $data['pk'] = $id;
                }
            }
            header('Content-Type: application/json;charset=UTF-8');
            echo CJSON::encode($data);
        }
    }

    public function actionControllers() {
        $dir = opendir(Yii::getPathOfAlias('application.controllers'));
        $files = array();
        if ($dir) {
            /* This is the correct way to loop over the directory. */
            while (false !== ($entry = readdir($dir))) {
                if($entry !== '.' AND $entry !== '..') {
                    $files[] = $entry;
                }
            }
            closedir($dir);
        }
        
        header('Content-Type: application/json;chartset=UTF-8');
        echo CJSON::encode($files);
    }
    
    public function actionController()
    {
        $dirPath = Yii::getPathOfAlias('application.controllers');
        $msg = CJSON::decode($this->payload);
        if(isset($msg['type'])) {
            $controller = $msg['type'];
            $file = file_get_contents($dirPath.'/'.$controller);
            $msg['data'] = $file;
            
            header('Content-Type: application/json;charset=UTF-8');
            echo CJSON::encode($msg);
        }
    }

    protected function storeModel($name, $attributes) {
        /* @var $m CActiveRecord */
        $m = new $name;
        $m->attributes = (array) $attributes;
        return ($m->save(false)) ? $m->primaryKey : null;
    }

    protected function updateModel($name, $attributes, $pk) {
        /* @var $m CActiveRecord */
        $m = $name::model()->findByPk($pk);
        if (!$m) {
            $m = new $name;
        }
        $m->attributes = (array) $attributes;
        return $m->save(false);
    }

    protected function removeModel($name, $pk) {
        /* @var $m CActiveRecord */
        $m = $name::model()->findByPk($pk);
        if ($m) {
            return $m->delete();
        }
        return false;
    }

}
